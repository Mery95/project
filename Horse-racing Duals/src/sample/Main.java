package sample;
import java.util.*;
import java.io.*;
import java.math.*;

class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        ArrayList<Integer> listPi = new ArrayList<Integer>();
        for (int i = 0; i < N; i++)
        {
            int pi = in.nextInt();
            System.err.println("#" + i + ": "+ pi);

            listPi.add(pi);
        }
        Collections.sort(listPi);

        int diff = Math.abs(listPi.get(0) - listPi.get(1));
        for (int i =1; i< N - 1; i++)
        {
            if (Math.abs(listPi.get(i) - listPi.get(i+1)) < diff)
                diff = Math.abs(listPi.get(i) - listPi.get(i+1));

        }

        

        System.out.println(diff);
    }
}